package com.brxnxx.maze.database;

import com.brxnxx.maze.database.DatabaseRunnable;
import com.brxnxx.maze.database.column.Column;
import com.brxnxx.maze.updater.UpdateEvent;
import com.brxnxx.maze.updater.UpdateType;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;

public abstract class RepositoryBase implements Listener {

    private static final Object queueLock = new Object();
    private HashMap<DatabaseRunnable, String> failedQueue = new HashMap<>();
    private DataSource dataSource;
    protected JavaPlugin plugin;

    public RepositoryBase(JavaPlugin plugin, DataSource dataSource) {
        this.plugin = plugin;
        this.dataSource = dataSource;

        Bukkit.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            initialize();
            update();
        });

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    protected abstract void initialize();

    protected abstract void update();

    protected DataSource getConnectionPool() {
        return dataSource;
    }

    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected int executeUpdate(String query, Column<?>... columns) {
        return executeInsert(query, null, columns);
    }

    protected int executeInsert(String query, ResultSetCallable callable, Column<?>... columns) {
        int affectedRows = 0;
        try (
                Connection connection = this.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)
        ) {
            for (int i = 0; i < columns.length; i++) {
                columns[i].setValue(preparedStatement, i + 1);
            }

            affectedRows = preparedStatement.executeUpdate();
            if (callable != null) {
                callable.processResultSet(preparedStatement.getGeneratedKeys());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return affectedRows;
    }

    protected void executeQuery(PreparedStatement statement, ResultSetCallable callable, Column<?>... columns) {
        try {
            for (int i = 0; i < columns.length; i++) {
                columns[i].setValue(statement, i + 1);
            }
            try (ResultSet resultSet = statement.executeQuery()) {
                callable.processResultSet(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void executeQuery(String query, ResultSetCallable callable, Column<?>... columns) {
        try (
                Connection connection = this.getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(query)
        ) {
            executeQuery(preparedStatement, callable, columns);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void handleDatabaseCall(final DatabaseRunnable databaseRunnable, final String errorMessage) {
        Thread asyncThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    databaseRunnable.run();
                } catch (Exception exception) {
                    processFailedDatabaseCall(databaseRunnable, exception.getMessage(), errorMessage);
                }
            }
        });
        asyncThread.start();
    }

    protected void processFailedDatabaseCall(DatabaseRunnable databaseRunnable, String errorPreMessage, String runnableMessage) {
        if (databaseRunnable.getFailedAttempts() < 4) {
            databaseRunnable.incrementFailedAttempts();
            synchronized (queueLock) {
                failedQueue.put(databaseRunnable, runnableMessage);
            }
        }
    }

    @EventHandler
    public void processDatabaseQueue(UpdateEvent event) {
        if (event.getType() != UpdateType.MIN_01)
            return;
        processFailedQueue();
    }

    private void processFailedQueue() {
        synchronized (queueLock) {
            for (DatabaseRunnable databaseRunnable : failedQueue.keySet()) {
                handleDatabaseCall(databaseRunnable, failedQueue.get(databaseRunnable));
            }
        }
    }



}
