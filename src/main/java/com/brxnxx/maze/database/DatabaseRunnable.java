package com.brxnxx.maze.database;

public class DatabaseRunnable {

    private Runnable runnable;
    private int failedAttempts = 0;

    public DatabaseRunnable(Runnable runnable) {
        this.runnable = runnable;
    }

    public void run() {
        this.runnable.run();
    }

    public void incrementFailedAttempts() {
        this.failedAttempts++;
    }

    public int getFailedAttempts() {
        return failedAttempts;
    }
}
