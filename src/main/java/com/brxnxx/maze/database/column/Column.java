package com.brxnxx.maze.database.column;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Column<Type> {

    public String name;
    public Type value;

    public Column(String name) {
        this.name = name;
    }

    public Column(String name, Type value) {
        this.name = name;
        this.value = value;
    }

    public abstract String getCreateString();

    public abstract Type getValue(ResultSet resultSet) throws SQLException;

    public abstract void setValue(PreparedStatement preparedStatement, int columnNumber) throws SQLException;

    public abstract Column<Type> clone();

}
