package com.brxnxx.maze.database.column;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnVarChar extends Column<String> {
    public int Length = 25;

    public ColumnVarChar(String name, int length) {
        this(name, length, "");
    }

    public ColumnVarChar(String name, int length, String value) {
        super(name);

        Length = length;
        this.value = value;
    }

    public String getCreateString() {
        return name + " VARCHAR(" + Length + ")";
    }

    @Override
    public String getValue(ResultSet resultSet) throws SQLException {
        return resultSet.getString(name);
    }

    @Override
    public void setValue(PreparedStatement preparedStatement, int columnNumber) throws SQLException {
        preparedStatement.setString(columnNumber, value);
    }

    @Override
    public ColumnVarChar clone() {
        return new ColumnVarChar(name, Length, value);
    }
}
