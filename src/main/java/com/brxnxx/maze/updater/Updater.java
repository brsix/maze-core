package com.brxnxx.maze.updater;

import org.bukkit.plugin.java.JavaPlugin;

public class Updater implements Runnable {

    public static void initialize(JavaPlugin plugin) {
        instance = new Updater(plugin);
    }

    public static Updater instance;

    public static Updater getInstance() {
        return instance;
    }

    private JavaPlugin plugin;

    public Updater(JavaPlugin plugin) {
        this.plugin = plugin;
        plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, this, 0L, 1L);
    }

    @Override
    public void run() {
        for (UpdateType updateType : UpdateType.values()) {
            if (updateType.elapsed()) {
                this.plugin.getServer().getPluginManager().callEvent(new UpdateEvent(updateType));
            }
        }
    }


}
