package com.brxnxx.maze.commands.interfaces;

import com.brxnxx.maze.account.enums.Group;
import com.brxnxx.maze.commands.CommandCenter;
import org.bukkit.entity.Player;

import java.util.Collection;

public interface ICommand {

    /**
     * Set command center
     *
     * @param commandCenter {@link CommandCenter}
     */
    void setCommandCenter(CommandCenter commandCenter);

    /**
     * Execute command
     *
     * @param player {@link Player}
     * @param args equals string vector
     */
    void execute(Player player, String[] args);

    /**
     * Command aliases
     *
     * @return {@link Collection<String>}
     */
    Collection<String> aliases();

    /**
     * Set alias
     *
     * @param name equals string
     */
    void setAliasUsed(String name);

    /**
     * Required group for execute command
     *
     * @return
     */
    Group getRequiredGroup();

}
