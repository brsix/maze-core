package com.brxnxx.maze.commands;

import com.brxnxx.maze.account.AccountManager;
import com.brxnxx.maze.commands.interfaces.ICommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.help.HelpTopic;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CommandCenter implements Listener {

    private AccountManager accountManager;

    public static CommandCenter instance;
    protected JavaPlugin javaPlugin;
    private Map<String, ICommand> commandsMap;

    public void initialize(JavaPlugin javaPlugin) {
        if (instance == null)
            instance = new CommandCenter(javaPlugin);
    }

    public CommandCenter(JavaPlugin javaPlugin) {
        this.javaPlugin = javaPlugin;
        this.commandsMap = new ConcurrentHashMap<>();
        this.javaPlugin.getServer().getPluginManager().registerEvents(this, javaPlugin);
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String commandName = event.getMessage().substring(1);
        String[] args = null;

        if (commandName.contains(" ")) {
            commandName = commandName.split(" ")[0];
            args = event.getMessage().substring(event.getMessage().indexOf(' ') + 1).split(" ");
        }

        if (event.getMessage().toLowerCase().startsWith("/me ")) {
            this.sendMessage(event.getPlayer(), "Esse comando foi desabilitado.");
            event.setCancelled(true);
        }

        if (event.getMessage().split(" ")[0].contains(":")) {
            this.sendMessage(event.getPlayer(), "Você não pode enviar comandos que possuem ':' (dois pontos)");
            event.setCancelled(true);
        }

        ICommand command = this.commandsMap.get(commandName.toLowerCase());
        if (command != null) {
            event.setCancelled(true);

            if (accountManager.getAccount(event.getPlayer()).getGroup().hasGroup(event.getPlayer(), command.getRequiredGroup(), true)) {
                command.setAliasUsed(commandName.toLowerCase());
                command.execute(event.getPlayer(), args);
            }
        }

        if (!event.isCancelled()) {
            String cmd = event.getMessage().split( " ")[0];
            HelpTopic topic = Bukkit.getHelpMap().getHelpTopic(cmd);
            if (topic == null) {
                this.sendMessage(event.getPlayer(), "Comando não encontrado.");
                event.setCancelled(true);
            }
        }
    }

    public void addCommand(ICommand command) {
        for (String commandRoot : command.aliases()) {
            commandsMap.put(commandRoot.toLowerCase(), command);
            command.setCommandCenter(this);
        }
    }

    public void removeCommand(ICommand command) {
        for (String commandRoot : command.aliases()) {
            commandsMap.remove(commandRoot.toLowerCase());
            command.setCommandCenter(null);
        }
    }

    private void sendMessage(Player player, String message) {
        player.sendMessage(ChatColor.BLUE + "Maze " + ChatColor.BOLD + "» " + ChatColor.GRAY + message);
    }

    public void setAccountManager(AccountManager accountManager) {
        this.accountManager = accountManager;
    }

}
