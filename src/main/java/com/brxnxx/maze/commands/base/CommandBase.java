package com.brxnxx.maze.commands.base;

import com.brxnxx.maze.account.enums.Group;
import com.brxnxx.maze.commands.CommandCenter;
import com.brxnxx.maze.commands.interfaces.ICommand;
import com.brxnxx.maze.server.Module;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public abstract class CommandBase<PluginType extends Module> implements ICommand {

    private Group requiredGroup;
    private List<String> aliases;

    public PluginType plugin;
    protected String aliasUsed;
    protected CommandCenter commandCenter;

    public CommandBase(PluginType plugin, Group requiredGroup, String... aliases) {
        this.plugin = plugin;
        this.requiredGroup = requiredGroup;
        this.aliases = Arrays.asList(aliases);
    }


    @Override
    public void setCommandCenter(CommandCenter commandCenter) {
        this.commandCenter = commandCenter;
    }

    public Collection<String> aliases() {
        return aliases;
    }

    public void setAliasUsed(String aliasUsed) {
        this.aliasUsed = aliasUsed;
    }

    @Override
    public Group getRequiredGroup() {
        return requiredGroup;
    }

    protected List<String> getMatches(String start, List<String> possibleMatches) {
        List<String> matches = new ArrayList<>();

        for (String possibleMatch : possibleMatches) {
            if (possibleMatch.toLowerCase().startsWith(start.toLowerCase()))
                matches.add(possibleMatch);
        }

        return matches;
    }

    @SuppressWarnings("rawtypes")
    protected List<String> getMatches(String start, Enum[] numerators) {
        List<String> matches = new ArrayList<>();

        for (Enum e : numerators) {
            String s = e.toString();
            if (s.toLowerCase().startsWith(start.toLowerCase()))
                matches.add(s);
        }

        return matches;
    }

    protected List<String> getPlayerMatches(Player sender, String start) {
        List<String> matches = new ArrayList<>();

        for (Player player : Bukkit.getOnlinePlayers()) {
            if (sender.canSee(player) && player.getName().toLowerCase().startsWith(start.toLowerCase())) {
                matches.add(player.getName());
            }
        }

        return matches;
    }

    public void sendMessage(Player player, String message) {
        player.sendMessage(ChatColor.BLUE + plugin.getModuleName() + ChatColor.BOLD + " » " + ChatColor.GRAY + message);
    }

}
