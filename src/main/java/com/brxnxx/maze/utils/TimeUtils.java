package com.brxnxx.maze.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtils {

    public static final String DATE_FORMAT_NOW = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_DAY = "dd/MM/yyyy";

    public static final SimpleDateFormat FORMAT_DAY = new SimpleDateFormat(DATE_FORMAT_DAY);
    public static final SimpleDateFormat FORMAT_NOW = new SimpleDateFormat(DATE_FORMAT_NOW);

    public static String now() {
        Calendar calendar = Calendar.getInstance();
        return FORMAT_DAY.format(calendar);
    }

    public static String when(long time) {
        return FORMAT_NOW.format(time);
    }

    public static String date() {
        Calendar calendar = Calendar.getInstance();
        return FORMAT_DAY.format(calendar.getTime());
    }

    public enum TimeUnit {
        FIT, DAYS, HOURS, MINUTES, SECONDS, MILLISECOND
    }

    public static String since(long epoch) {
        return convertString(System.currentTimeMillis() - epoch, 0, TimeUnit.FIT);
    }

    public static double convert(long time, int trim, TimeUnit type) {
        if (type == TimeUnit.FIT) {
            if (time < 60000) type = TimeUnit.SECONDS;
            else if (time < 3600000) type = TimeUnit.MINUTES;
            else if (time < 86400000) type = TimeUnit.HOURS;
            else type = TimeUnit.DAYS;
        }
        if (type == TimeUnit.DAYS) return MathUtils.trim(trim, (time) / 86400000d);
        if (type == TimeUnit.HOURS) return MathUtils.trim(trim, (time) / 3600000d);
        if (type == TimeUnit.MINUTES) return MathUtils.trim(trim, (time) / 60000d);
        if (type == TimeUnit.SECONDS) return MathUtils.trim(trim, (time) / 1000d);
        else return MathUtils.trim(trim, time);
    }

    public static String makeString(long time) {
        return makeString(time, 0);
    }

    public static String makeString(long time, int trim) {
        return convertString(MathUtils.max(0, time), trim, TimeUnit.FIT);
    }

    public static String convertString(long time, int trim, TimeUnit type) {
        if (time == -1) return "Permanente.";
        if (type == TimeUnit.FIT) {
            if (time < 60000) type = TimeUnit.SECONDS;
            else if (time < 3600000) type = TimeUnit.MINUTES;
            else if (time < 86400000) type = TimeUnit.HOURS;
            else type = TimeUnit.DAYS;
        }

        String text;
        double num;
        if (trim == 0) {
            if (type == TimeUnit.DAYS) text = (int) (num = (int) MathUtils.trim(trim, time / 86400000d)) + " dia";
            else if (type == TimeUnit.HOURS) text = (int) (num = (int) MathUtils.trim(trim, time / 3600000d)) + " hora";
            else if (type == TimeUnit.MINUTES)
                text = (int) (num = (int) MathUtils.trim(trim, time / 60000d)) + " minuto";
            else if (type == TimeUnit.SECONDS)
                text = (int) (num = (int) MathUtils.trim(trim, time / 1000d)) + " segundo";
            else text = (int) (num = (int) MathUtils.trim(trim, time)) + " milissegundo";
        } else {
            if (type == TimeUnit.DAYS) text = (num = MathUtils.trim(trim, time / 86400000d)) + " dia";
            else if (type == TimeUnit.HOURS) text = (num = MathUtils.trim(trim, time / 3600000d)) + " hora";
            else if (type == TimeUnit.MINUTES) text = (num = MathUtils.trim(trim, time / 60000d)) + " minuto";
            else if (type == TimeUnit.SECONDS) text = (num = MathUtils.trim(trim, time / 1000d)) + " segundo";
            else text = (int) (num = (int) MathUtils.trim(0, time)) + " milissegundo";
        }

        if (num != 1) {
            text += "s";
        }
        return text;
    }

    public static boolean elapsed(long from, long required) {
        return System.currentTimeMillis() - from > required;
    }

}
