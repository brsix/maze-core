package com.brxnxx.maze.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Random;

public class MathUtils {

    public static double trim(int degree, double d) {
        StringBuilder format = new StringBuilder("#.#");

        for (int i = 1; i < degree; i++)
            format.append("#");

        DecimalFormatSymbols symb = new DecimalFormatSymbols(Locale.US);
        DecimalFormat twoDForm = new DecimalFormat(format.toString(), symb);
        return Double.valueOf(twoDForm.format(d));
    }

    public static long max(long a, long b) {
        return (a >= b) ? a : b;
    }

    public static Random random = new Random();
    public static int random(int i) {
        return random.nextInt(i);
    }

}
