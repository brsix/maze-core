package com.brxnxx.maze.account;

import com.brxnxx.maze.account.command.UpdateGroup;
import com.brxnxx.maze.account.enums.Group;
import com.brxnxx.maze.server.Module;
import com.brxnxx.maze.updater.UpdateEvent;
import com.brxnxx.maze.updater.UpdateType;

import org.bukkit.Bukkit;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class AccountManager extends Module {

    public static AccountManager instance;
    private JavaPlugin plugin;
    private AccountRepository repository;
    private HashMap<String, Account> clientList;
    private HashSet<String> duplicateLoginGlitchPreventionList;

    private static AtomicInteger clientsConnection = new AtomicInteger(0);
    private static AtomicInteger clientsProcessing = new AtomicInteger(0);

    public AccountManager(JavaPlugin plugin) {
        super("Gerenciador de Contas", plugin);

        this.plugin = plugin;
        this.repository = new AccountRepository(plugin);
        this.clientList = new HashMap<>();
        this.duplicateLoginGlitchPreventionList = new HashSet<>();

    }

    public static void initialize(JavaPlugin plugin) {
        instance = new AccountManager(plugin);
    }

    @Override
    public void addCommands() {
        this.addCommand(new UpdateGroup(this));
    }

    public static AccountManager getInstance() {
        return instance;
    }

    public AccountRepository getRepository() {
        return repository;
    }

    public Account addAccount(String name) {
        for (Account account : clientList.values()) {
            if (account.getPlayerName().equalsIgnoreCase(name)) {
                return account;
            }
        }
        Account account = new Account(name);
        clientList.put(name, account);
        return account;
    }

    public void delAccount(String name) {
        clientList.remove(name);
    }

    public Account getAccount(String name) {
        return clientList.get(name);
    }

    public Account getAccount(Player player) {
        return this.getAccount(player.getName());
    }

    public int getPlayerCountIncludingConnecting() {
        return Bukkit.getOnlinePlayers().size() + Math.max(0, clientsConnection.get());
    }

    private boolean loadAccount(final Account account) {
        long timeStart = System.currentTimeMillis();
        account.setAccountId(repository.login(account.getPlayerName()));
        String rank = "NONE";
        try (
                Connection connection = repository.getConnection();
                Statement statement = connection.createStatement()
        ) {
            String query = ("SELECT rank FROM accounts WHERE playerName ='" + account.getPlayerName() + "';");
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                rank = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        account.setGroup(Group.valueOf(rank));
        return System.currentTimeMillis() - timeStart < 15000;
    }

    @EventHandler
    public void playerKickEvent(PlayerKickEvent event) {
        if (event.getReason().contains("You logged in from another location")) {
            duplicateLoginGlitchPreventionList.add(event.getPlayer().getName());
        }
    }

    @EventHandler
    public void cleanGlitchedAccounts(UpdateEvent event) {
        if (event.getType() != UpdateType.SLOW) {
            return;
        }

        for (Iterator<Map.Entry<String, Account>> accountIterator = clientList.entrySet().iterator(); accountIterator.hasNext(); ) {
            Player accountPlayer = accountIterator.next().getValue().getPlayer();
            if (accountPlayer != null && !accountPlayer.isOnline()) {
                accountIterator.remove();
            }
        }

    }


}
