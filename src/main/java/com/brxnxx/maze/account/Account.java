package com.brxnxx.maze.account;

import com.brxnxx.maze.account.enums.Group;
import org.bukkit.entity.Player;

public class Account {

    private int accountId = -1;
    private String playerName;
    private Player player;
    private Group group;

    public Account(Player player) {
        this.player = player;
        this.playerName = player.getName();
    }

    public Account(String name) {
        this.playerName = name;
    }

    public void delete() {
        player = null;
        playerName = null;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
