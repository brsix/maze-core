package com.brxnxx.maze.account;

import com.brxnxx.maze.account.enums.Group;
import com.brxnxx.maze.database.DBPool;
import com.brxnxx.maze.database.RepositoryBase;
import com.brxnxx.maze.database.column.ColumnVarChar;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AccountRepository extends RepositoryBase {

    private static String CREATE_ACCOUNT_TABLE = "CREATE TABLE IF NOT EXISTS accounts (id INT NOT NULL AUTO_INCREMENT, " +
            "playerName VARCHAR(100), rank VARCHAR(50), lastLogin LONG, totalPlayTime LONG, " +
            "PRIMARY KEY (id), UNIQUE INDEX playerNameIndex (playerName), INDEX groupIndex (rank));";
    private static String INSERT_NEW_PLAYER = "INSERT INTO accounts (playerName, rank, lastLogin) VALUES (?, 'NONE', now());";
    private static String UPDATE_ACCOUNT_GROUP = "UPDATE accounts SET rank=? WHERE playerName = ?;";


    public AccountRepository(JavaPlugin plugin) {
        super(plugin, DBPool.DATABASE);
    }

    @Override
    protected void initialize() {
        this.executeUpdate(CREATE_ACCOUNT_TABLE);
    }

    int login(String name) {
        int accountId = -1;
        try (
                Connection connection = this.getConnection();
                Statement statement = connection.createStatement();
        ) {

            if (!this.playerExists(name)) {
                this.executeInsert(INSERT_NEW_PLAYER, null, new ColumnVarChar("playerName", 100, name));
            }

            statement.execute("SELECT id FROM accounts WHERE playerName = '" + name + "' LIMIT 1;");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                accountId = resultSet.getInt(1);
            }

            statement.execute("UPDATE accounts SET lastLogin=now() WHERE id='" + accountId + "';");
            statement.getUpdateCount();
            statement.getMoreResults();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountId;
    }

    public void updateGroup(String name, Group group) {
        this.executeUpdate(UPDATE_ACCOUNT_GROUP, new ColumnVarChar("rank", 50, group.toString()), new ColumnVarChar("playerName", 100, name));
        if (Bukkit.getPlayer(name) != null) {
            Account account = AccountManager.getInstance().getAccount(name);
            account.setGroup(group);
        }
    }

    public boolean playerExists(String playerName) {
        int accountId = -1;
        try (
                Connection connection = this.getConnection();
                Statement statement = connection.createStatement()
        ) {
            statement.execute("SELECT id FROM accounts WHERE accounts.playerName ='" + playerName + "' LIMIT 1;");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                accountId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return accountId > 0;
    }

    @Override
    protected void update() {

    }
}
