package com.brxnxx.maze.account.command;

import com.brxnxx.maze.account.AccountManager;
import com.brxnxx.maze.account.enums.Group;
import com.brxnxx.maze.commands.base.CommandBase;

import org.bukkit.entity.Player;

public class UpdateGroup extends CommandBase<AccountManager> {

    public UpdateGroup(AccountManager plugin) {
        super(plugin, Group.ADMIN, "setgroup");
    }

    @Override
    public void execute(Player player, String[] args) {
        if (args == null) {
            this.sendMessage(player, "Uso: /" + aliasUsed + " [jogador] [grupo]");
            return;
        }

        if (args.length < 2) {
            this.sendMessage(player, "Uso: /" + aliasUsed + " [jogador] [grupo]");
            return;
        }

        String playerName = args[0];
        Group group;
        try {
            group = Group.valueOf(args[1]);
        } catch (Exception ex) {
            this.sendMessage(player, "Grupo inválido.");
            return;
        }

        if (plugin.getAccount(player).getGroup() == group && !plugin.getAccount(player).getGroup().hasGroup(Group.MASTER)) {
            this.sendMessage(player, "Você não pode definir um grupo igual ao seu.");
            return;
        }

        if (!plugin.getAccount(player).getGroup().hasGroup(group)) {
            this.sendMessage(player,"Você não pode definir um grupo maior que o seu.");
            return;
        }

        if (!plugin.getRepository().playerExists(playerName)) {
            this.sendMessage(player, "O jogador nunca entrou no servidor.");
            return;
        }
        plugin.getRepository().updateGroup(playerName, group);
        this.sendMessage(player,  "Grupo de " + playerName + " foi atualizado com sucesso.");
    }
}
