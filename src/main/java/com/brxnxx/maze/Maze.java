package com.brxnxx.maze;

import com.brxnxx.maze.account.AccountManager;
import com.brxnxx.maze.commands.CommandCenter;
import com.brxnxx.maze.updater.Updater;
import org.bukkit.plugin.java.JavaPlugin;

public class Maze extends JavaPlugin {

    private static Maze core;

    @Override
    public void onLoad() {
        core = this;
        core.getLogger().warning("Loading core features...");
    }

    @Override
    public void onEnable() {
        core.getLogger().warning("Enabling core features...");

        CommandCenter.instance.initialize(core);
        AccountManager.initialize(core);
        CommandCenter.instance.setAccountManager(AccountManager.getInstance());

        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Updater(this), 1, 1);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static Maze getCore() {
        return core;
    }

}
