package com.brxnxx.maze.server;

import com.brxnxx.maze.commands.CommandCenter;
import com.brxnxx.maze.commands.interfaces.ICommand;
import com.brxnxx.maze.utils.TimeUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public abstract class Module implements Listener {

    private String moduleName;
    private JavaPlugin javaPlugin;
    private Map<String, ICommand> commandsMap;

    public Module(String moduleName, JavaPlugin javaPlugin) {
        this.moduleName = moduleName;
        this.javaPlugin = javaPlugin;
        commandsMap = new ConcurrentHashMap<>();
        this.onEnable();
    }

    public void enable() {
    }

    public void disable() {
    }

    public void addCommands() {
    }

    public final void addCommand(ICommand command) {
        CommandCenter.instance.addCommand(command);
    }

    public final void removeCommand(ICommand command) {
        CommandCenter.instance.removeCommand(command);
    }

    public PluginManager getPluginManager() {
        return javaPlugin.getServer().getPluginManager();
    }

    public BukkitScheduler getScheduler() {
        return javaPlugin.getServer().getScheduler();
    }

    public JavaPlugin getPlugin() {
        return javaPlugin;
    }

    public void registerEvents(Listener listener) {
        this.getPluginManager().registerEvents(listener, javaPlugin);
    }

    public void unregisterEvents(Listener listener) {
        HandlerList.unregisterAll(listener);
    }

    public Logger getLogger() {
        return javaPlugin.getLogger();
    }

    public final void onEnable() {
        long start = System.currentTimeMillis();
        this.info("Habilitando...");
        this.registerEvents(this);
        addCommands();
        this.info("Habilitado em " + TimeUtils.convertString(System.currentTimeMillis() - start, 3, TimeUtils.TimeUnit.MILLISECOND));
    }

    private void info(String information) {
        this.getLogger().info(moduleName + "> " + information);
    }

    public void runTaskAsynchronously(Runnable runnable) {
        this.getScheduler().runTaskAsynchronously(javaPlugin, runnable);
    }

    public void runTask(Runnable runnable) {
        this.getScheduler().runTask(javaPlugin, runnable);
    }

    public void runTaskLater(Runnable runnable, long delay) {
        this.getScheduler().runTaskLater(javaPlugin, runnable, delay);
    }

    public void sendMessage(Player player, String message) {
        player.sendMessage(ChatColor.BLUE + moduleName + ChatColor.BOLD + " » "  + ChatColor.GRAY + message);
    }

    public Map<String, ICommand> getCommandsMap() {
        return commandsMap;
    }

    public String getModuleName() {
        return moduleName;
    }

    public JavaPlugin getJavaPlugin() {
        return javaPlugin;
    }

}
